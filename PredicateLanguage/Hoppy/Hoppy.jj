options {
  LOOKAHEAD = 1;
  STATIC = true;
  CHOICE_AMBIGUITY_CHECK = 2;
  OTHER_AMBIGUITY_CHECK = 1;
  DEBUG_PARSER = false;
  DEBUG_LOOKAHEAD = false;
  DEBUG_TOKEN_MANAGER = false;
  ERROR_REPORTING = true;
  JAVA_UNICODE_ESCAPE = false;
  UNICODE_INPUT = false;
  IGNORE_CASE = false;
  USER_TOKEN_MANAGER = false;
  USER_CHAR_STREAM = false;
  BUILD_PARSER = true;
  BUILD_TOKEN_MANAGER = true;
  SANITY_CHECK = true;
  FORCE_LA_CHECK = false;
}

PARSER_BEGIN(Hoppy)

package predvis.hoppy;

import java.util.*;
import java.io.*;

public class Hoppy
{
	public static void main(String args[]) throws Exception, ParseException, CompilationException, TypeException
	{
		Hoppy parser = new Hoppy(System.in);

		HashMap<Integer, Integer> vds = new HashMap<Integer, Integer>();

		parser.compile(System.out, vds);
		
		for (int key : vds.keySet())
		{
			System.out.println("//VD " + key + " = " + vds.get(key));
		}
	}

	public String compile(OutputStream out, HashMap<Integer, Integer> vds) throws Exception
	{
		Program program = Input();

        PrintStream ps = new PrintStream(out);

		ps.println("//" + program.toString());
		ps.println("//TARGETING " + program.getPredicateTarget());

        Compiler c = new Compiler(ps);
		c.visit(program.getSyntaxTree());

		if (vds != null)
		{
			vds.putAll(c.getVariableDetails());
		}
		
		return program.getPredicateTarget().toString();
	}
}

class CompilationException extends Exception
{
	public CompilationException(String message)
	{
		super(message);
	}
}

final class NotInScopeException extends CompilationException
{
	public NotInScopeException(String name)
	{
		super("The variable called " + name + " is not in the scope");
	}
}

final class AlreadyInScopeException extends CompilationException
{
	public AlreadyInScopeException(String name)
	{
		super("The variable called " + name + " is already in the scope");
	}
}

class TypeException extends Exception
{
	public TypeException()
	{
		super();
	}

	public TypeException(String message)
	{
		super(message);
	}

	public TypeException(ISyntaxTree tree, ExprType obtained, ExprType expected)
	{
		super("Expected " + tree + " to be of type " + expected + " was of type " + obtained);
	}

	public TypeException(ISyntaxTree tree, ExprType obtained)
	{
		super("Incorrectly obtained the type of " + tree + " to be " + obtained);
	}
}


enum ExprType
{
	Boolean, Integer, Float, User, Set, Function
}


final class Compiler implements ISyntaxTreeVisitor
{
	class ArrIds
	{ 
		public final int id; 
		public final int arrid; 
		public ArrIds(int id, int arrid) { 
			this.id = id; 
			this.arrid = arrid; 
		} 
	}

	class FunDetails
	{ 
		public final int id; 
		public final ExprType type; 
		public FunDetails(int id, ExprType type) { 
			this.id = id; 
			this.type = type; 
		} 
	}

	private static final HashMap<String, String> setTransBuiltins = new HashMap<String, String>();
	private static final HashMap<String, String> setBuiltins = new HashMap<String, String>();
	private static final HashMap<String, String> mathBuiltins = new HashMap<String, String>();

	static
	{
		setTransBuiltins.put("sum", "ASUM");
		setTransBuiltins.put("mean", "AMEAN");
		setTransBuiltins.put("max", "AMAX");
		setTransBuiltins.put("min", "AMIN");

		setBuiltins.put("len", "ALEN");

		mathBuiltins.put("abs", "_ABS");
	}

	private final HashMap<String, FunDetails> functionScope = new HashMap<String, FunDetails>();
	private final HashMap<String, ArrIds> variableScope = new HashMap<String, ArrIds>();
	private final HashMap<String, Integer> arrayScope = new HashMap<String, Integer>();
	private final HashMap<Integer, Integer> neighbourVarDetails = new HashMap<Integer, Integer>();

	private int variableId = 0;
	private int arrVariableId = 255;

	private boolean writtenHalt = false;

    private final PrintStream out;
	
	public HashMap<Integer, Integer> getVariableDetails()
	{
		return neighbourVarDetails;
	}
    
    public Compiler()
    {
		this(System.out);
    }
	
	public Compiler(PrintStream out)
	{
		// The this variable is implicitly in the scope
		variableScope.put("this", new ArrIds(variableId++, 0));

        this.out = out;
	}
	
	public Compiler(Compiler other)
	{
		this(other.out);
		this.variableScope.putAll(other.variableScope);
		this.arrayScope.putAll(other.arrayScope);
		this.functionScope.putAll(other.functionScope);
		this.neighbourVarDetails.putAll(other.neighbourVarDetails);
		this.variableId = other.variableId;
		this.writtenHalt = other.writtenHalt;
	}

	private void write(String code)
	{
		out.println(code);
	}

	private void write(String code, String label)
	{
		out.println(code + " " + label);
	}

	private void write(String code, int arg)
	{
		out.println(code + " " + arg);
	}

	private void write(String code, int arg1, int arg2)
	{
		out.println(code + " " + arg1 + " " + arg2);
	}

	private void write(String code, int arg1, int arg2, int arg3)
	{
		out.println(code + " " + arg1 + " " + arg2 + " " + arg3);
	}

	private void write(String code, float arg)
	{
		out.println(code + " " + arg);
	}

	private void label(String l)
	{
		out.print(l + ": ");
	}

	private void comment(String c)
	{
		out.println("//" + c);
	}


	private ExprType type(ISyntaxTree tree) throws TypeException
	{
		if (tree instanceof NamedValueNode)
		{
			return type((NamedValueNode)tree);
		}
		else if (tree instanceof LiteralValueNode)
		{
			return type((LiteralValueNode)tree);
		}
		else if (tree instanceof UnaryOperatorNode)
		{
			return type((UnaryOperatorNode)tree);
		}
		else if (tree instanceof BinaryOperatorNode)
		{
			return type((BinaryOperatorNode)tree);
		}
		else if (tree instanceof QuantifierNode)
		{
			return type((QuantifierNode)tree);
		}
		else if (tree instanceof FuncDecl)
		{
			return type((FuncDecl)tree);
		}
		else if (tree instanceof UsingDecl)
		{
			return null;
		}
		else if (tree instanceof FunDefDecl)
		{
			return null;
		}
		else
		{
			// Unknown class
			//System.err.println("Unknown class " + tree);
			throw new TypeException();
		}
	}

	private ExprType type(NamedValueNode tree) throws TypeException
	{
		if (tree.getName().equals("this") || variableScope.containsKey(tree.getName()))
		{
			return ExprType.User;
		}
		else if (arrayScope.containsKey(tree.getName()))
		{
			return ExprType.Set;
		}
		else if (functionScope.containsKey(tree.getName()))
		{
			return ExprType.Function;
		}
		else
		{
			// Most likely we haven't visited a quantifier node yet
			// so the variable name is not the the variableScope
			return ExprType.User;
			//throw new TypeException("Unsure of type of " + tree);
		}
	}

	private ExprType type(LiteralValueNode tree)
	{
		return tree.getVal().type();
	}

	private ExprType type(UnaryOperatorNode tree) throws TypeException
	{
		if (type(tree.getChild()) != ExprType.Boolean)
			throw new TypeException(tree.getChild(), type(tree.getChild()), ExprType.Boolean);

		return ExprType.Boolean;
	}

	private ExprType type(BinaryOperatorNode tree) throws TypeException
	{
		boolean isLogical = tree.getOp().isLogical();
		boolean isLogicalInput = tree.getOp().isLogicalInput();

		if (isLogical && isLogicalInput)
		{
			if (type(tree.getLeft()) != ExprType.Boolean)
				throw new TypeException(tree.getLeft(), type(tree.getLeft()), ExprType.Boolean);

			if (type(tree.getRight()) != ExprType.Boolean)
				throw new TypeException(tree.getRight(), type(tree.getRight()), ExprType.Boolean);

			return ExprType.Boolean;
		}
		else if (isLogical && !isLogicalInput)
		{
			if (type(tree.getLeft()) != ExprType.Integer && type(tree.getLeft()) != ExprType.Float)
				throw new TypeException();

			if (type(tree.getRight()) != ExprType.Integer && type(tree.getRight()) != ExprType.Float)
				throw new TypeException();

			return ExprType.Boolean;
		}
		else
		{
			if (type(tree.getLeft()) == ExprType.Integer && type(tree.getRight()) == ExprType.Integer)
			{
				return ExprType.Integer;
			}
			else if (type(tree.getLeft()) == ExprType.Float || type(tree.getRight()) == ExprType.Float)
			{
				return ExprType.Float;
			}
			else
			{
				throw new TypeException();
			}
		}
	}

	private ExprType type(QuantifierNode tree) throws TypeException
	{
		if (type(tree.getPredicate()) != ExprType.Boolean)
			throw new TypeException();

		return ExprType.Boolean;
	}

	private ExprType type(FuncDecl tree) throws TypeException
	{
		if (functionScope.containsKey(tree.getName()))
		{
			if (type(tree.getArgs()[0]) != ExprType.User)
				throw new TypeException(tree.getArgs()[0], type(tree.getArgs()[0]), ExprType.User);

			return functionScope.get(tree.getName()).type;
		}
		else if (setTransBuiltins.containsKey(tree.getName()))
		{
			if (type(tree.getArgs()[0]) != ExprType.Set)
				throw new TypeException(tree.getArgs()[0], type(tree.getArgs()[0]), ExprType.Set);

			if (type(tree.getArgs()[1]) != ExprType.Function)
				throw new TypeException(tree.getArgs()[1], type(tree.getArgs()[1]), ExprType.Function);

			return ExprType.Float;
		}
		else if (setBuiltins.containsKey(tree.getName()))
		{
			if (type(tree.getArgs()[0]) != ExprType.Set)
				throw new TypeException(tree.getArgs()[0], type(tree.getArgs()[0]), ExprType.Set);

			return ExprType.Integer;
		}
		else if (mathBuiltins.containsKey(tree.getName()))
		{
			if (type(tree.getArgs()[0]) != ExprType.Integer && type(tree.getArgs()[0]) != ExprType.Float)
				throw new TypeException();

			return type(tree.getArgs()[0]);
		}
		else
		{
			throw new TypeException();
		}
	}


	public void visit(ISyntaxTree tree) throws Exception
	{
		if (tree instanceof NamedValueNode)
		{
			throw new CompilationException("Cannot call NamedValueNode");
		}
		else if (tree instanceof LiteralValueNode)
		{
			LiteralValueNode item = (LiteralValueNode)tree;

			IValue value = item.getVal();

			if (value instanceof BooleanValue)
			{
				BooleanValue bval = (BooleanValue)value;
				write("IPUSH", bval.getValue() ? 1 : 0);
			}
			else if (value instanceof IntegerValue)
			{
				IntegerValue bval = (IntegerValue)value;
				write("IPUSH", bval.getValue());
			}
			else if (value instanceof FloatValue)
			{
				FloatValue bval = (FloatValue)value;
				write("FPUSH", bval.getValue());
			}
			else
			{
				throw new CompilationException("Not sure how to generate code for: " + tree);
			}
		}
		else if (tree instanceof UnaryOperatorNode)
		{
			UnaryOperatorNode item = (UnaryOperatorNode)tree;

			item.getChild().acceptVisit(copy());

			if (type(item.getChild()) != ExprType.Boolean)
				throw new TypeException(item.getChild(), type(item.getChild()), ExprType.Boolean);

			write(item.getBytecode());
		}
		else if (tree instanceof BinaryOperatorNode)
		{
			BinaryOperatorNode item = (BinaryOperatorNode)tree;

			ExprType l = type(item.getLeft()), r = type(item.getRight());

			String bytecode = item.getBytecode();

			if (bytecode.startsWith("_"))
			{
				if (l == ExprType.Integer && r == ExprType.Integer)
				{
					bytecode = bytecode.replace('_', 'I');
				}
				else if (l == ExprType.Float || r == ExprType.Float)
				{
					bytecode = bytecode.replace('_', 'F');
				}
				else
				{
					throw new TypeException(item, type(item));
				}
			}

			boolean floatResult = bytecode.startsWith("F");

			item.getLeft().acceptVisit(copy());

			if (floatResult && l == ExprType.Integer)
			{
				write("ICASTF");
			}

			item.getRight().acceptVisit(copy());

			if (floatResult && r == ExprType.Integer)
			{
				write("ICASTF");
			}

			write(bytecode);
		}
		else if (tree instanceof QuantifierNode)
		{
			QuantifierNode item = (QuantifierNode)tree;
			
			checkIs(arrayScope.keySet(), item.getSet());

			int arrayId = arrayScope.get(item.getSet());

			int loopCounterId = variableId++;

			String startlabel = "start" + loopCounterId;
			String endlabel = "end" + loopCounterId;
			
			// Make sure not in scope before we add it
			checkIsNot(variableScope.keySet(), item.getVar());
			variableScope.put(item.getVar(), new ArrIds(loopCounterId, arrayId));


			QuantifierNode.Quantifier q = item.getQuantifier();


			boolean all = q == QuantifierNode.Quantifier.FOR_ALL;

			// Allocate the variable id
			write("IVAR", loopCounterId);
			
			// First we need to push a variable that will store the result
			// This should be 1 for "ALL" and 0 for "EXIST"
			write("IPUSH", all ? 1 : 0);


			// Now we need to initialise the loop counter
			write("IPUSH", 0);
			write("ISTORE", loopCounterId);


			// Perform loop termination check
			label(startlabel); write("ALEN", arrayId);
			write("INEQ");
			write("JZ", endlabel);

			// Perform body operations
			item.getPredicate().acceptVisit(copy());

			// Combine this iteration's result with the result
			// stack variable
			if (all)
			{
				write("AND");
			}
			else
			{
				write("OR");
			}

			//// Increment loop counter
			write("VIINC", loopCounterId);

			// Jump to start of loop
			write("JMP", startlabel);

			// Loop termination
			label(endlabel);
		}
		else if (tree instanceof FuncDecl)
		{
			FuncDecl item = (FuncDecl)tree;

			if (setBuiltins.keySet().contains(item.getName()))
			{
				if (item.getArgs().length != 1)
					throw new CompilationException("Expected 1 arguments for a set function");

				ISyntaxTree arg = item.getArgs()[0];

				if (arg instanceof NamedValueNode)
				{
					String argstr = ((NamedValueNode)arg).getName();

					checkIs(arrayScope.keySet(), argstr);

					int varid = arrayScope.get(argstr);

					write(setBuiltins.get(item.getName()), varid);
				}
				else
				{
					throw new CompilationException("Parameters to set fn are not strings");
				}
			}
			else if (mathBuiltins.keySet().contains(item.getName()))
			{
				if (item.getArgs().length != 1)
					throw new CompilationException("Expected 1 arguments for a set function");

				for (ISyntaxTree arg : item.getArgs())
				{
					arg.acceptVisit(copy());
				}

				String bytecode = mathBuiltins.get(item.getName());

				if (bytecode.startsWith("_"))
				{
					if (type(item) == ExprType.Integer)
					{
						bytecode = bytecode.replace('_', 'I');
					}
					else if (type(item) == ExprType.Float)
					{
						bytecode = bytecode.replace('_', 'F');
					}
					else
					{
						throw new TypeException(item, type(item));
					}
				}

				write(bytecode);
			}
			else if (setTransBuiltins.keySet().contains(item.getName()))
			{
				if (item.getArgs().length != 2)
					throw new CompilationException("Expected 2 arguments for a set transformation function");

				ISyntaxTree arg1 = item.getArgs()[0];
				ISyntaxTree arg2 = item.getArgs()[1];

				if (arg1 instanceof NamedValueNode && arg2 instanceof NamedValueNode)
				{
					String arg1str = ((NamedValueNode)arg1).getName();
					String arg2str = ((NamedValueNode)arg2).getName();

					checkIs(arrayScope.keySet(), arg1str);
					checkIs(functionScope.keySet(), arg2str);

					int varid = arrayScope.get(arg1str);
					int fnid = functionScope.get(arg2str).id;

					write(setTransBuiltins.get(item.getName()), varid, fnid);
				}
				else
				{
					throw new CompilationException("Parameters to set trans fn are not strings");
				}
			}
			else if (functionScope.keySet().contains(item.getName()))
			{
				if (item.getArgs().length != 1)
					throw new CompilationException("Expected 1 arguments for a function");

				// Can only call these functions on a name
				// A name can only be used in these cases
				if (item.getArgs()[0] instanceof NamedValueNode)
				{
					NamedValueNode nvn = (NamedValueNode)item.getArgs()[0];

					checkIs(variableScope.keySet(), nvn.getName());

					int fnid = functionScope.get(item.getName()).id;

					if (nvn.getName().equals("this"))
					{
						comment(item.getName() + "(this)");
						write("THISC", fnid);
					}
					else
					{
						ArrIds vid = variableScope.get(nvn.getName());

						// Equivalent to:
						// IFETCH id
						// AFETCH arrid
						// CALL fnid
						comment(item.getName() + "(" + nvn.getName() + "[*" + vid.id + "])");
						write("VIFAFC", vid.id, vid.arrid, fnid);
					}
				}
				else
				{
					throw new CompilationException("Expected set function to have a named argument");
				}
			}
			else
			{
				throw new CompilationException("Not sure what to do with " + item + " as a function.");
			}
		}
		else if (tree instanceof UsingDecl)
		{
			UsingDecl item = (UsingDecl)tree;

			int location = arrVariableId--;

			comment("STORING " + item.getNeighbourCount() + " IN " + item.getVariable());
			
			neighbourVarDetails.put(item.getNeighbourCount(), location);
			
			// Make sure not in scope before we add it
			checkIsNot(arrayScope.keySet(), item.getVariable());
			arrayScope.put(item.getVariable(), location);

			boolean writeHalt = false;
			if (!writtenHalt)
			{
				writeHalt = writtenHalt = true;
			}
			
			// Don't create a copy as we want these changes to be visible to the original
			item.getScope().acceptVisit(this);

			if (writeHalt)
			{
				// Write a halt just in case we have a quantifier
				// that has no operations after it
				write("HALT");
			}
		}
		else if (tree instanceof FunDefDecl)
		{
			FunDefDecl item = (FunDefDecl)tree;

			comment("FUNC " + item.getId() + " AS " + item.getName());

			// Make sure not in scope before we add it
			checkIsNot(functionScope.keySet(), item.getName());

			functionScope.put(item.getName(), new FunDetails(item.getId(), item.getType()));

			// Don't create a copy as we want these changes to be visible to the original
			item.getScope().acceptVisit(this);
		}
		else
		{
			// Unknown class
			throw new CompilationException("Unknown class " + tree);
		}
	}
	
	private ISyntaxTreeVisitor copy()
	{
		return new Compiler(this);
	}
	
	private static void checkIs(Set<String> items, String name) throws NotInScopeException
	{
		if (!items.contains(name))
			throw new NotInScopeException(name);
	}
	
	private static void checkIsNot(Set<String> items, String name) throws AlreadyInScopeException
	{
		if (items.contains(name))
			throw new AlreadyInScopeException(name);
	}
}



interface IValue
{
	String toString();

	ExprType type();
}

final class BooleanValue implements IValue
{
	private boolean value;
	
	public BooleanValue(String value) throws ParseException
	{
		this.value = Boolean.parseBoolean(value);
	}
	
	public BooleanValue(boolean value)
	{
		this.value = value;
	}

	public boolean getValue() { return value; }
	public void setValue(boolean value) { this.value = value; }

	@Override
	public String toString()
	{
		return "B:" + value;
	}

	public ExprType type()
	{
		return ExprType.Boolean;
	}

	public static BooleanValue assertType(IValue val) throws TypeException
	{
		if(!(val instanceof BooleanValue))
		{
			throw new TypeException("Value type not boolean.");
		}
		else
		{
			return (BooleanValue)val;
		}
	}
}

final class IntegerValue implements IValue
{
	private int value;
	
	public IntegerValue(String value) throws ParseException
	{
		this.value = Integer.parseInt(value);
	}
	
	public IntegerValue(int value)
	{
		this.value = value;
	}

	public int getValue() { return value; }
	public void setValue(int value) { this.value = value; }

	@Override
	public String toString()
	{
		return "I:" + value;
	}

	public ExprType type()
	{
		return ExprType.Integer;
	}

	public static IntegerValue assertType(IValue val) throws TypeException
	{
		if(!(val instanceof IntegerValue))
		{
			throw new TypeException("Value type not integer.");
		}
		else
		{
			return (IntegerValue)val;
		}
	}
}

final class FloatValue implements IValue
{
	private float value;
	
	public FloatValue(String value) throws ParseException
	{
		this.value = Float.parseFloat(value);
	}
	
	public FloatValue(float value)
	{
		this.value = value;
	}

	public float getValue() { return value; }
	public void setValue(float value) { this.value = value; }

	@Override
	public String toString()
	{
		return "F:" + value;
	}

	public ExprType type()
	{
		return ExprType.Float;
	}

	public static FloatValue assertType(IValue val) throws TypeException
	{
		if(!(val instanceof FloatValue))
		{
			throw new TypeException("Value type not float.");
		}
		else
		{
			return (FloatValue)val;
		}
	}
}


interface ISyntaxTreeVisitor
{
	void visit(ISyntaxTree tree) throws Exception;
}

/**
 * Base class for syntax tree nodes.
 */
interface ISyntaxTree
{
	String toString();
	
	void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception;
}


/**
 * Concrete node class representing a named value.
 */
class NamedValueNode implements ISyntaxTree
{
	private final String name;

	public NamedValueNode(String name)
	{
		this.name = name;
	}
	
	public String getName() { return name; }

	@Override
	public String toString()
	{
		return "N:" + name;
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}
}

/**
 * Concrete node class representing a literal value.
 */
class LiteralValueNode implements ISyntaxTree
{
	private IValue val;

	public LiteralValueNode(IValue val)
	{
		this.val = val;
	}

	public IValue getVal() { return val; }
	public void setVal(IValue val) { this.val = val; }

	@Override
	public String toString()
	{
		return val.toString();
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}
}

/**
 * Concrete node class representing a unary operator.
 */
class UnaryOperatorNode implements ISyntaxTree
{
	public enum Operator
	{
		NOT("NOT");

		private final String code;

		private Operator(String code)
		{
			this.code = code;
		}

		public String getBytecode()
		{
			return code;
		}
	}

	private Operator op;
	private ISyntaxTree child;
	
	public UnaryOperatorNode(String name, ISyntaxTree child) throws ParseException
	{
		this(parseOperator(name), child);
	}

	public UnaryOperatorNode(Operator op, ISyntaxTree child)
	{
		this.op = op;
		this.child = child;
	}

	public Operator getOp() { return op; }
	public void setOp(Operator op) { this.op = op; }

	public ISyntaxTree getChild() { return child; }
	public void setChild(ISyntaxTree child) { this.child = child; }

	public String getBytecode()
	{
		return op.getBytecode();
	}

	@Override
	public String toString()
	{
		return "(" + child.toString() + " " + op.toString() + ")";
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}

	public static Operator parseOperator(String op) throws ParseException
	{
		if(op == "!")	return Operator.NOT;
		else			throw new ParseException("Expected `!'");
	}
}

/**
 * Concrete node class representing an binary operator.
 */
class BinaryOperatorNode implements ISyntaxTree
{
	public enum Operator
	{
		EQUAL("_EQ", true, false),
		NOT_EQUAL("_NEQ", true, false),
		LESS_THAN("_LT", true, false),
		LESS_THAN_EQUAL("_LEQ", true, false),
		GREATER_THAN("_GT", true, false),
		GREATER_THAN_EQUAL("_GEQ", true, false),
		
		AND("AND", true, true),
		OR("OR", true, true),
		XOR("XOR", true, true),
		IMPLIES("IMPLIES", true, true),
		EQUIVALENT("EQUIVALENT", true, true),

		PLUS("_ADD", false, false),
		MINUS("_SUB", false, false),
		MULTIPLY("_MUL", false, false),
		DIVIDE("_DIV2", false, false),
		POWER("_POW", false, false);

		private final String code;
		private final boolean logical;
		private final boolean logicalInput;

		private Operator(String code, boolean logical, boolean logicalInput)
		{
			this.code = code;
			this.logical = logical;
			this.logicalInput = logicalInput;
		}

		public String getBytecode()
		{
			return code;
		}

		public boolean isLogical()
		{
			return logical;
		}

		public boolean isLogicalInput()
		{
			return logicalInput;
		}
	}

	private Operator op;
	private ISyntaxTree left;
	private ISyntaxTree right;
	
	public BinaryOperatorNode(String name, ISyntaxTree left, ISyntaxTree right) throws ParseException
	{
		this(parseOperator(name), left, right);
	}

	public BinaryOperatorNode(Operator op, ISyntaxTree left, ISyntaxTree right)
	{
		this.op = op;
		this.left = left;
		this.right = right;
	}

	public Operator getOp() { return op; }
	public void setOp(Operator op) { this.op = op; }
	
	public ISyntaxTree getLeft() { return left; }
	public void setLeft(ISyntaxTree left) { this.left = left; }

	public ISyntaxTree getRight() { return right; }
	public void setRight(ISyntaxTree right) { this.right = right; }

	public String getBytecode()
	{
		return op.getBytecode();
	}

	@Override
	public String toString()
	{
		return "(" + left.toString() + " " + right.toString() + " " + op.toString() + ")";
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}

	public static Operator parseOperator(String op) throws ParseException
	{
		if(op.equals("=="))			return Operator.EQUAL;
		else if(op.equals("!="))	return Operator.NOT_EQUAL;
		else if(op.equals("<"))		return Operator.LESS_THAN;
		else if(op.equals("<="))	return Operator.LESS_THAN_EQUAL;
		else if(op.equals(">"))		return Operator.GREATER_THAN;
		else if(op.equals(">="))	return Operator.GREATER_THAN_EQUAL;

		else if(op.equals("+"))		return Operator.PLUS;
		else if(op.equals("-"))		return Operator.MINUS;
		else if(op.equals("*"))		return Operator.MULTIPLY;
		else if(op.equals("/"))		return Operator.DIVIDE;
		else if(op.equals("**"))	return Operator.POWER;

		else if(op.equals("&"))		return Operator.AND;
		else if(op.equals("|"))		return Operator.OR;
		else if(op.equals("^"))		return Operator.XOR;
		else if(op.equals("=>"))	return Operator.IMPLIES;
		else if(op.equals("<=>"))	return Operator.EQUIVALENT;

		else						throw new ParseException("Unknown operator: " + op);
	}
}

class QuantifierNode implements ISyntaxTree
{
	public enum Quantifier
	{
		FOR_ALL,
		EXISTS
	}

	private Quantifier quantifier;
	private String var;
	private String set;
	private ISyntaxTree predicate;

	
	public QuantifierNode(String name, String var, String set, ISyntaxTree predicate) throws ParseException
	{
		this(parseQuantifier(name), var, set, predicate);
	}
	
	public QuantifierNode(Quantifier quantifier, String var, String set, ISyntaxTree predicate)
	{
		this.quantifier = quantifier;
		this.var = var;
		this.set = set;
		this.predicate = predicate;
	}

	public Quantifier getQuantifier() { return quantifier; }
	public void setQuantifier(Quantifier quantifier) { this.quantifier = quantifier; }
	
	public String getVar() { return var; }
	public void setVar(String var) { this.var = var; }
	
	public String getSet() { return set; }
	public void setSet(String set) { this.set = set; }

	public ISyntaxTree getPredicate() { return predicate; }
	public void setPredicate(ISyntaxTree predicate) { this.predicate = predicate; }

	@Override
	public String toString()
	{
		return "(" + quantifier.toString() + " " + var + " IN " + set + " DO " + predicate.toString() + ")";
	}

	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}

	public static Quantifier parseQuantifier(String quantifier) throws ParseException
	{
		if(quantifier.equals("@"))		return Quantifier.FOR_ALL;
		else if(quantifier.equals("#"))	return Quantifier.EXISTS;
		else							throw new ParseException("Unknown quantifier: " + quantifier);
	}
}

final class FuncDecl implements ISyntaxTree
{
	private final String name;
	private final ISyntaxTree[] args;

	public FuncDecl(String name, ISyntaxTree[] args)
	{
		this.name = name;
		this.args = args;
	}

	public String getName() { return name; }
	public ISyntaxTree[] getArgs() { return args; }

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("(");

		if (args.length > 0)
		{
			sb.append(args[0]);

			for (int i = 1; i < args.length; ++i)
			{
				sb.append(" ");
				sb.append(args[i]);
			}
		}

		sb.append(" F:" + name + ")");

		return sb.toString();
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}
}

final class UsingDecl implements ISyntaxTree
{
	private int neighbourCount = 0;
	private String variable = null;
	private ISyntaxTree scope = null;

	public String getVariable() { return variable; }
	public void setVariable(String variable) { this.variable = variable; }

	public int getNeighbourCount() { return neighbourCount; }
	public void setNeighbourCount(int neighbourCount) { this.neighbourCount = neighbourCount; }
	
	public ISyntaxTree getScope() { return scope; }
	public void setScope(ISyntaxTree scope) { this.scope = scope; }

	@Override
	public String toString()
	{
		return "(USING " + neighbourCount + " AS " + variable + " IN " + scope.toString() + ")";
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}
}

final class FunDefDecl implements ISyntaxTree
{
	private int id = 0;
	private String name = null;
	private ExprType type = null;
	private ISyntaxTree scope = null;

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public ExprType getType() { return type; }
	public void setType(ExprType type) { this.type = type; }
	
	public ISyntaxTree getScope() { return scope; }
	public void setScope(ISyntaxTree scope) { this.scope = scope; }

	@Override
	public String toString()
	{
		return "(FUNDEF " + id + " AS " + name + " RETURNING " + type + " IN " + scope.toString() + ")";
	}
	
	public void acceptVisit(ISyntaxTreeVisitor visitor) throws Exception
	{
		visitor.visit(this);
	}
}

final class PredicateTarget
{
	private boolean all = false;
	private int[] addr = null;

	public void setToAll() { all = true; addr = null; }
	public boolean isToAll() { return all; }

	public void setAddr(int[] addr) { this.addr = addr; all = false; }
	public int[] getAddr() { return addr; }

	@Override
	public String toString()
	{
		return all ? "all" : addr[0] + "." + addr[1];
	}
}

final class Program
{
	private final PredicateTarget target;
	private final ISyntaxTree tree;
	
	public Program(PredicateTarget target, ISyntaxTree tree)
	{
		this.target = target;
		this.tree = tree;
	}
	
	public PredicateTarget getPredicateTarget() { return target; }
	public ISyntaxTree getSyntaxTree() { return tree; }
	
	@Override
	public String toString()
	{
		return "[" + target.toString() + "] " + tree.toString();
	}
}

PARSER_END(Hoppy)

TOKEN : {
	// Keywords
	  < USING : "using" >
	| < AS :	"as" >
	| < IN : 	"in" >
	| < THIS : 	"this" >
	| < ALL : 	"all" >
	| < COLON :	":" >
	| < TILDE :	"~" >

	| < FUNCTION :	"function" >
	| < RETURNING :	"returning" >
	| < TYPE :		"int" | "float" >

	// Functions
	| < NEIGHBOURS :	"Neighbours" >

	// Set functions
	| < SET_TRANS_FNS :	"sum" | "mean" | "max" | "min" >
	| < SET_FNS : 		"len" >

	// Variable functions
	| < VAR_FNS :		"abs" >
	
	// High precedence operators have a higher number prefix after
	// them here. However, when parsing the lowest numbered prefix
	// is used first.
	
	// Operators
	| < LOGICAL_MATH_OP_1 : "<" | "<=" | ">" | ">=" >
	| < LOGICAL_MATH_OP_2 : "==" | "!=" >
	
	| < MATH_OP_1 : 		"**" >
	| < MATH_OP_2 : 		"*" | "/" | "%" >
	| < MATH_OP_3 : 		"+" | "-" >
	
	| < LOGICAL_BIN_OP_1 :	"&" >
	| < LOGICAL_BIN_OP_2 :	"^" >
	| < LOGICAL_BIN_OP_3 :	"|" >
	| < LOGICAL_BIN_OP_4 :	"=>" >
	| < LOGICAL_BIN_OP_5 :	"<=>" >
	
	| < LOGICAL_UN_OP :		"!" >
	| < QUANTIFIER :		"@" | "#" >
	
	// Regexes
	| < NUMBER :	("-")? (<DIGIT>)+ >
	| < FLOAT :		("-")? (<DIGIT>)+ ("." (<DIGIT>)*)? >
	| < NAME :		<LETTER> (<ALPHANUM>)* >
	| < ALPHANUM : 	<LETTER> | <DIGIT> >
	| <#LETTER :	["A"-"Z", "a"-"z"] >
	| <#DIGIT :		["0"-"9"] >

	| < LB :	"(" >
	| < RB :	")" >

	| < SLB :	"[" >
	| < SRB :	"]" >

	| < COMMA :	"," >
}

SKIP : {
	  " "
	| "\t"
	| "\n"
	| "\r"
	
	// Skip comment lines
	| <"//" (~["\n","\r"])* ("\n"|"\r")>
}

Program Input() :
{
	PredicateTarget target;
	ISyntaxTree tree;
}
{
	<SLB>
	(
		target = Target()
	)
	<SRB>
	(
		tree = FunctionDefinition()
	)
	<EOF>
	{
		return new Program(target, tree);
	}
}

PredicateTarget Target() :
{
	int a, b;
	int[] addr = null;
}
{
	(
		(
			// Flood targeting.
			<ALL>
		)
		|
		(
			// Specific node targeting.
			(a = UnsignedCharNumber() <COMMA> b = UnsignedCharNumber())
		)
		{
			addr = new int[] { a, b };
		}
	)
	{
		PredicateTarget target = new PredicateTarget();

		if (addr == null) 
		{
			target.setToAll();
		}
		else
		{
			target.setAddr(addr);
		}

		return target;
	}
}

int UnsignedCharNumber() :
{
	Token t;
}
{
	(t = <NUMBER>)
	{
		int i = Integer.parseInt(t.image);

		if (!(i >= 0 && i <= 255))
		{
			throw new ParseException("Integer is not in desired format");
		}

		return i;
	}
}


ISyntaxTree FunctionDefinition() :
{
	Token id, name, type;

	FunDefDecl root = null, current = null;
	ISyntaxTree node;
}
{
	(
		(
			<FUNCTION>
			(
				id = <NUMBER>
			)
			<AS>
			(
				name = <NAME>
			)
			<RETURNING>
			(
				type = <TYPE>
			)
			<IN>
		)
		{
			FunDefDecl decl = new FunDefDecl();
			decl.setId(Integer.parseInt(id.image));
			decl.setName(name.image);

			if (type.image.equals("int"))
				decl.setType(ExprType.Integer);
			else if (type.image.equals("float"))
				decl.setType(ExprType.Float);
			else
				throw new ParseException("Unknown function type `" + type.image + "'");
			
			if (root == null)
			{
				current = root = decl;
			}
			else
			{
				current.setScope(decl);
				
				current = decl;
			}
		}
	)* 

	node = Using()
	{
		if (current == null)
		{
			return node;
		}
		else
		{
			current.setScope(node);
			
			return root;
		}
	}
} 

ISyntaxTree Using() :
{
	int count;
	Token location, fnAlias;

	UsingDecl root = null, current = null;
	ISyntaxTree node;
}
{
	(
		(
			<USING> 
			(
				count = Neighbours()
			)
			<AS>
			(
				fnAlias = <NAME>
			) 
			<IN>
		)
		{
			UsingDecl usingdecl = new UsingDecl();
			usingdecl.setNeighbourCount(count);
			usingdecl.setVariable(fnAlias.image);
			
			if (root == null)
			{
				current = root = usingdecl;
			}
			else
			{
				current.setScope(usingdecl);
				
				current = usingdecl;
			}
		}
	)* 

	node = Predicate()
	{
		if (current == null)
		{
			return node;
		}
		else
		{
			current.setScope(node);
			
			return root;
		}
	}
} 

int Neighbours() :
{
	Token argToken;
}
{
	<NEIGHBOURS> <LB> (argToken = <NUMBER>) <RB>
	{
		return Integer.parseInt(argToken.image);
	}
}

ISyntaxTree Predicate() :
{
	ISyntaxTree node;
}
{
	node = LogicalBinaryExprFirst() { return node; }
}

ISyntaxTree QuantifierExpr() :
{
	Token quantifier;
	Token var;
	Token set;
	ISyntaxTree left;
}
{
	quantifier = <QUANTIFIER>
	<LB>
	var = <NAME>
	<COLON>
	set = <NAME>
	<TILDE>
	left = Predicate()
	<RB>
	{
		return new QuantifierNode(quantifier.image, var.image, set.image, left);
	}
}


ISyntaxTree LogicalUnaryExpr() :
{
	Token op;
	ISyntaxTree left;
}
{
	//A unary operator applied to a predicate.
	op = <LOGICAL_UN_OP>
	left = Predicate()
	{
		return new UnaryOperatorNode(op.image, left);
	}
}


ISyntaxTree LogicalMathBinaryExprFirst() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A logical comparison between two arithmetic expressions.
	(left = LogicalMathBinaryExprSecond())
	(
		(op = <LOGICAL_MATH_OP_2>) (right = LogicalMathBinaryExprSecond())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree LogicalMathBinaryExprSecond() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A logical comparison between two arithmetic expressions.
	(left = MathBinaryExprFirst())
	(
		(op = <LOGICAL_MATH_OP_1>) (right = MathBinaryExprFirst())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree MathBinaryExprFirst() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A logical comparison between two arithmetic expressions.
	(left = MathBinaryExprSecond())
	(
		(op = <MATH_OP_3>) (right = MathBinaryExprSecond())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree MathBinaryExprSecond() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A logical comparison between two arithmetic expressions.
	(left = MathBinaryExprThird())
	(
		(op = <MATH_OP_2>) (right = MathBinaryExprThird())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree MathBinaryExprThird() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A logical comparison between two arithmetic expressions.
	(left = VariableExpr())
	(
		(op = <MATH_OP_1>) (right = VariableExpr())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}


ISyntaxTree LogicalBinaryExprFirst() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A conjunction or disjunction on two predicates.
	(left = LogicalBinaryExprSecond())
	(
		(op = <LOGICAL_BIN_OP_5>) (right = LogicalBinaryExprSecond())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree LogicalBinaryExprSecond() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A conjunction or disjunction on two predicates.
	(left = LogicalBinaryExprThird())
	(
		(op = <LOGICAL_BIN_OP_4>) (right = LogicalBinaryExprThird())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree LogicalBinaryExprThird() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A conjunction or disjunction on two predicates.
	(left = LogicalBinaryExprFourth())
	(
		(op = <LOGICAL_BIN_OP_3>) (right = LogicalBinaryExprFourth())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree LogicalBinaryExprFourth() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A conjunction or disjunction on two predicates.
	(left = LogicalBinaryExprFifth())
	(
		(op = <LOGICAL_BIN_OP_2>) (right = LogicalBinaryExprFifth())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}

ISyntaxTree LogicalBinaryExprFifth() :
{
	Token op;
	ISyntaxTree left, right;
}
{
	//A conjunction or disjunction on two predicates.
	(left = LogicalBinaryExprEnd())
	(
		(op = <LOGICAL_BIN_OP_1>) (right = LogicalBinaryExprEnd())
		{
			left = new BinaryOperatorNode(op.image, left, right);
		}
	)*
	{
		return left;
	}
}


ISyntaxTree LogicalBinaryExprEnd() :
{
	ISyntaxTree node;
}
{
	node = QuantifierExpr() { return node; }
	|
	node = LogicalUnaryExpr() { return node; }
	|
	node = LogicalMathBinaryExprFirst() { return node; }
}


ISyntaxTree VariableExpr() :
{
	ISyntaxTree node;
}
{
	node = FunctionLiteral() { return node; }
	|
	node = ArithmeticLiteral() { return node; }
	|
	node = MathFunction() { return node; }
	|
	node = SetFunction() { return node; }
}

ISyntaxTree FunctionLiteral() :
{
	Token varname, varargs;
}
{
	// A name (either a variable, or if arguments are supplied, a function).
	varname = <NAME>
	(
		<LB>
		(
			(varargs = <THIS>) | (varargs = <NAME>)
		)
		<RB>
	)
	{
		return new FuncDecl(varname.image, new ISyntaxTree[] { new NamedValueNode(varargs.image) });
	}
}

ISyntaxTree ArithmeticLiteral() throws NumberFormatException, ParseException :
{
	Token literal;
}
{
	// An arithmetic literal.
	((literal = <NUMBER>) | (literal = <FLOAT>))
	{
		LiteralValueNode lvn = null;
	
		// Parse the literal and wrap it in a node.
		try
		{
			lvn = new LiteralValueNode(new IntegerValue(literal.image));
		}
		catch (NumberFormatException e)
		{
			lvn = new LiteralValueNode(new FloatValue(literal.image));
		}
		
		return lvn;
	}
}

ISyntaxTree MathFunction() :
{
	ISyntaxTree expr;
	Token varname;
}
{
	(
		varname = <VAR_FNS> <LB> expr = MathBinaryExprFirst() <RB>
		{
			return new FuncDecl(varname.image, new ISyntaxTree[] { expr });
		}
	)
}


ISyntaxTree SetFunction() :
{
	Token setfnname, varname, fnname;
}
{
	(
		setfnname = <SET_FNS> <LB> varname = <NAME> <RB>
		{
			return new FuncDecl(setfnname.image, new ISyntaxTree[] { new NamedValueNode(varname.image) });
		}
	)
	|
	(
		setfnname = <SET_TRANS_FNS> <LB> varname = <NAME> <COMMA> fnname = <NAME> <RB>
		{
			return new FuncDecl(setfnname.image, new ISyntaxTree[] { new NamedValueNode(varname.image), new NamedValueNode(fnname.image) });
		}
	)
}

