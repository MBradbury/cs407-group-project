#ifndef CS407_ARRAY_LIST
#define CS407_ARRAY_LIST

#ifndef _MSC_VER
#	include <stdbool.h>
#else
#	define bool int
#	define true 1
#	define false 0
#endif

typedef void (*array_list_cleanup_t)(void * data);

typedef unsigned int array_list_elem_t;

typedef struct array_list
{
	// List memory
	void ** data;

	// Size of reserved memory
	unsigned int length;

	// Number of elements in list
	unsigned int count;

	array_list_cleanup_t cleanup;
} array_list_t;

// Create the list
bool array_list_init(array_list_t * list, array_list_cleanup_t cleanup);
bool array_list_free(array_list_t * list);

// Add / Remove items from list
bool array_list_append(array_list_t * list, void * data);
bool array_list_clear(array_list_t * list);

bool array_list_remove(array_list_t * list, array_list_elem_t elem);

#ifdef CONTAINERS_CHECKED

#	define array_list_length(list) \
		((list) == NULL ? 0u : (list)->count)

#	define array_list_first(list) \
		((array_list_elem_t)0)

#	define array_list_next(elem) \
		((array_list_elem_t)(elem + 1))

#	define array_list_continue(list, elem) \
		((list) != NULL && (elem) < (list)->count)

#	define array_list_data(list, elem) \
		(((list) == NULL || (elem) >= (list)->count) \
			? NULL : (list)->data[(elem)])

#else

#	define array_list_length(list) \
		((list)->count)

#	define array_list_first(list) \
		((array_list_elem_t)0)

#	define array_list_next(elem) \
		((array_list_elem_t)(elem + 1))

#	define array_list_continue(list, elem) \
		((elem) < (list)->count)

#	define array_list_data(list, elem) \
		((list)->data[(elem)])

#endif

// Iteration example:
// array_list_t list;
// array_list_elem_t elem;
// for (elem = array_list_first(&list); array_list_continue(&list, elem); elem = array_list_next(elem))
// {
//     void * data = array_list_data(&list, elem);
// }

#endif

