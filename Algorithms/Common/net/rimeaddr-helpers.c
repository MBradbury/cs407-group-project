#include "rimeaddr-helpers.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

bool rimeaddr_pair_equality(void const * left, void const * right)
{
	if (left == NULL || right == NULL)
		return false;

	rimeaddr_pair_t const * lp = (rimeaddr_pair_t const *)left;
	rimeaddr_pair_t const * rp = (rimeaddr_pair_t const *)right;

	return
		(rimeaddr_cmp(&lp->first, &rp->first) && rimeaddr_cmp(&lp->second, &rp->second)) ||
		(rimeaddr_cmp(&lp->second, &rp->first) && rimeaddr_cmp(&lp->first, &rp->second));
}

bool rimeaddr_equality(void const * left, void const * right)
{
	if (left == NULL || right == NULL)
		return false;

	return rimeaddr_cmp((rimeaddr_t const *)left, (rimeaddr_t const *)right);
}

void * rimeaddr_clone(void const * addr)
{
	rimeaddr_t * ptr = (rimeaddr_t *)malloc(sizeof(rimeaddr_t));
	rimeaddr_copy(ptr, (rimeaddr_t const *)addr);
	return ptr;
}

void * rimeaddr_pair_clone(void const * addrs)
{
	rimeaddr_pair_t * ptr = (rimeaddr_pair_t *)malloc(sizeof(rimeaddr_pair_t));
	memcpy(ptr, addrs, sizeof(rimeaddr_pair_t));
	return ptr;
}
