\documentclass[a4paper,notitlepage]{article}

\usepackage[top=2cm, bottom=2cm, left=3cm, right=3cm]{geometry}

\usepackage{amssymb, amsmath}
\newtheorem{mydef}{Definition}

\usepackage{boxedminipage}
% This needs to occur before hyperref and minted
\usepackage{float}

\usepackage[usenames,dvipsnames]{xcolor}

\usepackage{multirow}

\usepackage[hyphens]{url}
\usepackage{hyperref}

\usepackage{appendix}
\usepackage[numbers]{natbib}

\usepackage{graphicx, subfigure}
\usepackage{epstopdf}

\usepackage{tabularx}
\usepackage{longtable}

\usepackage{dirtree}

\usepackage{comment}

\usepackage[firstpage]{draftwatermark}
\SetWatermarkText{\includegraphics[width=\paperwidth,angle=-45]{Images/wallpaper}}

% Use minted and set the visual style to be the
% same as the one used by visual studio
\usepackage{minted}
\usemintedstyle{vs}

% Allows use of autoref with lables in listings
\providecommand*{\listingautorefname}{Listing}

\usepackage{caption}

\newcommand{\mysideways}[1]{\begin{sideways}#1\end{sideways}}

\usepackage[parfill]{parskip}
\setlength{\parindent}{0pt}
\setlength{\parskip}{0.75\baselineskip}

% A better tilde (~)
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$} }

% For distributed algorithms
\newcommand{\res}[1]{\mbox{\textbf{#1}}}% for reserved words
\newcommand{\qq}{\qquad}% for spacing
\newcommand{\assign}{\mathrel{:=}}% for assigning variables
\newcommand{\var}[1]{\mbox{\emph{#1}}}% for variables

% For use in the title page
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% Predicate language syntax highlighting
\usepackage{listings}
\definecolor{keywordblue}{RGB}{20,105,176}
\lstdefinelanguage{Hoppy}{
    morekeywords=[1]{all,function,as,returning,in,using},
    morekeywords=[2]{this},
    morekeywords=[3]{int,float},
    morekeywords=[4]{Neighbours,abs,mean,max,min,sum,len},
    sensitive=true,
}
\lstset{
    language=Hoppy,
    keywordstyle=[1]\color{keywordblue},
    keywordstyle=[2]\color{OliveGreen},
    keywordstyle=[3]\color{YellowOrange},
    keywordstyle=[4]\color{Purple},
    basicstyle=\ttfamily,
    columns=fullflexible,
}

\lstdefinelanguage{Dragon}{
    morekeywords=[1]{IPUSH,IPOP,IFETCH,ISTORE,IADD,ISUB,IMUL,IDIV1,IDIV2,IINC,IDEC,IEQ,INEQ,ILT,ILEQ,IGT,IGEQ,IABS,IVAR,ICASTF},
    morekeywords=[2]{FPUSH,FPOP,FFETCH,FSTORE,FADD,FSUB,FMUL,FDIV1,FDIV2,FEQ,FNEQ,FLT,FLEQ,FGT,FGEQ,FABS,FVAR,FCASTI},
    morekeywords=[3]{AFETCH,ALEN,ASUM,AMEAN,AMAX,AMIN},
    morekeywords=[4]{HALT,CALL,JMP,JZ,JNZ},
    morekeywords=[5]{VIINC,VIDEC,VIFAFC,THISC},
    morekeywords=[6]{AND,OR,XOR,EQUIVALENT,IMPLIES,NOT},
    morecomment=[l]{//},
    sensitive=true,
}
\lstset{
    language=Dragon,
    keywordstyle=[1]\color{Cyan},
    keywordstyle=[2]\color{WildStrawberry},
    keywordstyle=[3]\color{YellowOrange},
    keywordstyle=[4]\color{Violet},
    keywordstyle=[5]\color{ForestGreen},
    keywordstyle=[6]\color{BurntOrange},
    basicstyle=\ttfamily,
    columns=fullflexible,
}

\newcommand{\sourcecode}[3]{%
\inputminted[linenos=true,tabsize=4,fontsize=\small,frame=lines,framesep=2mm]{c}{#1/#2}
\inputminted[linenos=true,tabsize=4,fontsize=\small,frame=lines,framesep=2mm]{c}{#1/#3}
}


% For \begin{acknowledgements}
% From: http://www.latex-community.org/forum/viewtopic.php?f=47&t=5464
\makeatletter
\newcommand\ackname{Acknowledgements}
\if@titlepage
  \newenvironment{acknowledgements}{%
      \titlepage
      \null\vfil
      \@beginparpenalty\@lowpenalty
      \begin{center}%
        \bfseries \ackname
        \@endparpenalty\@M
      \end{center}}%
     {\par\vfil\null\endtitlepage}
\else
  \newenvironment{acknowledgements}{%
      \if@twocolumn
        \section*{\abstractname}%
      \else
        \small
        \begin{center}%
          {\bfseries \ackname\vspace{-.5em}\vspace{\z@}}%
        \end{center}%
        \quotation
      \fi}
      {\if@twocolumn\else\endquotation\fi}
\fi
\makeatother


% From: http://nepsweb.co.uk/docs/bnf.pdf
% For writing predicate language productions
\newcommand{\pn}[1]{\langle \textnormal{#1} \rangle}
\newcommand{\pp}{\models}
\newcommand{\oo}{\; \mid \;}
\newcommand{\sk}{\dots }
\newcommand{\ww}{\;}
\newcommand{\nn}{\perp}
\newcommand{\sm}[1]{\textnormal{#1}}
\newcommand{\sd}[1]{\textnormal{\it #1}}

\newcommand{\smlst}[1]{\textnormal{\lstinline[language=Hoppy]|#1|}}

\begin{document}

\begin{titlepage}
\begin{center}

% Upper part of the page
\includegraphics[scale=0.6]{the_warwick_uni_blue.eps}\\[0.75cm]

{\LARGE \bfseries Department of Computer Science}\\[1.5cm] 

{\Large \bfseries Fourth Year Project}\\[0.5cm]

% Title
\HRule \\[0.4cm]
{\Huge \bfseries Towards Practical Debugging of Wireless Sensor Network Applications\par}
%\\[0.4cm]

\HRule \\[8.5cm]

\vfill
\vfill
\vfill
\vfill

% Author and supervisor
\noindent{
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Authors:}\\
Matthew Bradbury (0921660) \\
Tim Law (0918647) \\
Ivan Leong (0830934) \\
Daniel Robertson (0910210) \\
Amit Shah (0904778) \\
Joe Yarnall (0905247)\\
\end{flushleft}
\end{minipage}}
\hfill
\noindent{
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervisor:} \\
Dr.~Arshad Jhumka
\end{flushright}
\end{minipage}}

\vfill
\vfill
\vfill
\vfill
\vfill

% Bottom of the page
{\large October 2012 - May 2013}

\end{center}
\end{titlepage}

%No numbering on first and second pages
\pagestyle{empty}
\thispagestyle{empty}\clearpage

\newpage

\begin{abstract}
Debugging tools are vital for developers to produce reliable software, however traditional tools are less useful when developing software for new system paradigms such as wireless sensor networks. As wireless sensor networks (WSNs) become increasing prevalent in our lives it will become ever more important that the software they are running works reliably and to do this debugging tools will be required. This project investigates how predicates can be specified and accurately checked throughout WSNs and how errors can be reported to a base station. We also develop a system that facilitates reporting predicate statuses to a desktop application.
\newline
\newline
\noindent \textbf{Keywords} - Wireless Sensor Networks; Debugging; Reliability; Predicate Checking;
\end{abstract}

%\begin{acknowledgements}
%Acknowledgements
%\end{acknowledgements}

\clearpage


\pagestyle{plain}
\setcounter{page}{1}

\tableofcontents
\clearpage

\input{Introduction}
\clearpage

%\input{LiteratureReview}
%\clearpage

\input{Model}
\clearpage

\input{Predicates}
\clearpage

\input{ImplementedAlgorithms}
\clearpage

\input{Results}
\clearpage

\input{PracticalExperience}
\clearpage

\input{ProjectManagement}
\clearpage

\input{FutureWork}
\clearpage

\input{Conclusions}
\clearpage


\appendixpage
\addappheadtotoc
\appendix

\input{DeviceSpecifications}

\newpage

\section{Algorithm Implementations}

\subsection{Multipacket}
\sourcecode{../Algorithms/Common/net}{multipacket.h}{multipacket.c}

\subsection{Tree Aggregation}
\sourcecode{../Algorithms/Common/net}{tree-aggregator.h}{tree-aggregator.c}

\subsection{N-Hop Request}
\sourcecode{../Algorithms/Common/net}{nhopreq.h}{nhopreq.c}

\subsection{N-Hop Flood}
\sourcecode{../Algorithms/Common/net}{nhopflood.h}{nhopflood.c}

\subsection{Event Update}
\sourcecode{../Algorithms/Common/net}{eventupdate.h}{eventupdate.c}

\subsection{Hop Data Manager}
\sourcecode{../Algorithms/PredEval}{hop-data-manager.h}{hop-data-manager.c}

\subsection{Predicate Manager}
\sourcecode{../Algorithms/PredEval}{predicate-manager.h}{predicate-manager.c}

\subsection{PELP}
\sourcecode{../Algorithms/PredEvalLocalPeriodic}{pelp.h}{pelp.c}

\subsection{PELE}
\sourcecode{../Algorithms/PredEvalLocalEvent}{pele.h}{pele.c}

\subsection{PEGP}
\sourcecode{../Algorithms/PredEvalGlobalPeriodic}{pegp.h}{pegp.c}

\subsection{PEGE}
\sourcecode{../Algorithms/PredEvalGlobalEvent}{pege.h}{pege.c}


\newpage


\section{References}
\renewcommand{\refname}{\vspace{-1cm}}
\bibliographystyle{myplainnat}
\bibliography{../References/references,../References/BradburyCS310}


\end{document}
